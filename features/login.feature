# To run this, we have to write :
# behave features/login.feature
# in the terminal
# if we want to run every test in the folder, we use
# behave ./features

# Took: X minutes X seconds
@every_scenario
Feature: Login using the Android application

# Tests page in Confluence: http://confluence.ad.mirgor.com.ar:8090/confluence/x/CICIFQ
# Valid username and passwords: federico.capdeville@mirgor.com.ar | 12345a
# Another valid username and passwords (not used in this program): tenant@thingsboard.org      | 5THn85sWUUesumb
# Another valid username and passwords (not used in this program): nahuel.albornoz@incluit.com | pass123


  @finished
  Scenario Outline: Login in the application - non valid parameters
    Given Open the application
    When  Enter email "<email>" and password "<password>"
    Then  Click continue with non valid parameters
    And   Close the application

    Examples:
      | email                                | password   |
      | false                                | false      |
      | false                                | 12345a     |
      | federico.capdeville@mirgor.com.ar    | false      |
      | federico.capdeville@mirgor.com.ar123 | false      |
      | false                                | 12345a456  |
      | federico.capdeville@mirgor.com.ar123 | 12345a456  |

  @finished
  Scenario: Login in the application - blank email and password
    Given Open the application
    When  Enter email and password - blank components
    Then  Click continue with blank components
    And   Close the application

  @finished
  Scenario: Login in the application - email OK and password in blank
    Given Open the application
    When  Enter email OK and password in blank
    Then  Click continue with blank components
    And   Close the application

  @finished
  Scenario: Login in the application - email in blank and password OK
    Given Open the application
    When  Enter email in blank and password OK
    Then  Click continue with blank components
    And   Close the application

  @finished
  Scenario: Login in the application - happy path
    Given Open the application
    When  Enter valid username and password
    Then  Click continue with valid parameters
    And   Close the application
