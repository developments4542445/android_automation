# Appium documentation: https://appium.io/docs/en/about-appium/api/

from behave import *
from my_designs import *
from appium import webdriver


@given(u'Open the application')
def step_impl(context):
    print()
    try:
        context.driver = {
            "platformName": "Android",
            "plaftormVersion": "11",
            "deviceName": "rav",
            "automationName": "UiAutomator2",
            "appPackage": "com.mirgor.aliciahome",
            "appActivity": ".ui.MainActivity"
        }
        context.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', context.driver)
    except Exception as e:
        assert False, e

    test_finished()


@when(u'Enter email "{email}" and password "{password}"')
def step_impl(context, email, password):
    print()

    try:
        # Timer to avoid the splash screen
        print('Avoid the splash screen timer')
        delay(5)

        print('Print email in application')
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_email").click()
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_email").send_keys(email)

        print('Print password in application')
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_password").click()
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_password").send_keys(password)

        print('Click to enable/disable the view of the password')
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/text_input_end_icon").click()
        false_type_password = context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_password").\
            get_attribute("password")
        print("Password attribute is " + false_type_password)
        assert false_type_password == "false", "Password visualization is not correct: " + false_type_password

        context.driver.find_element_by_id("com.mirgor.aliciahome:id/text_input_end_icon").click()
        true_type_password = context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_password").\
            get_attribute("password")
        print("Password attribute is " + true_type_password)
        assert true_type_password == "true", "Password visualization is not correct: " + true_type_password

        print("Go back to avoid visible keyboard")
        context.driver.back()
    except Exception as e:
        assert False, e

    test_finished()


@then(u'Click continue with non valid parameters')
def step_impl(context):
    print()

    try:
        print('Click to continue')
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/button_login").click()

        print("Pop up generated: incorrect email / password")
        delay(1)
#        delay_until_appear(context.driver, "android:id/button3")               # issue has not appeared again
        context.driver.find_element_by_id("android:id/button3").click()
        delay(1)
    except Exception as e:
        assert False, e

    test_finished()


@then(u'Close the application')
def step_impl(context):
    print()

    try:
        context.driver.quit()
    except Exception as e:
        assert False, e

    test_finished()


@when(u'Enter email and password - blank components')
def step_impl(context):
    print()

    try:
        # Timer to avoid the splash screen
        print('Avoid the splash screen timer')
        delay(5)

        print('Let email in blank in application')
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_email").click()

        print('Let password in blank in application')
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_password").click()

        print("Go back to avoid visible keyboard")
        context.driver.back()
    except Exception as e:
        assert False, e

    test_finished()


@then(u'Click continue with blank components')
def step_impl(context):
    print()

    try:
        print('Click to continue')
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/button_login").click()
        delay(1)
    except Exception as e:
        assert False, e

    test_finished()


@when(u'Enter email OK and password in blank')
def step_impl(context):
    email = "federico.capdeville@mirgor.com.ar"
    print()

    try:
        # Timer to avoid the splash screen
        print('Avoid the splash screen timer')
        delay(5)

        print('Print email in application')
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_email").click()
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_email").send_keys(email)

        print('Let password in blank in application')
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_password").click()

        print("Go back to avoid visible keyboard")
        context.driver.back()
    except Exception as e:
        assert False, e

    test_finished()


@when(u'Enter email in blank and password OK')
def step_impl(context):
    password = "12345a"
    print()

    try:
        # Timer to avoid the splash screen
        print('Avoid the splash screen timer')
        delay(5)

        print('Let email in blank in application')
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_email").click()

        print('Print password in application')
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_password").click()
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_password").send_keys(password)

        print('Click to enable/disable the view of the password')
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/text_input_end_icon").click()
        false_type_password = context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_password"). \
            get_attribute("password")
        print("Password attribute is " + false_type_password)
        assert false_type_password == "false", "Password visualization is not correct: " + false_type_password

        context.driver.find_element_by_id("com.mirgor.aliciahome:id/text_input_end_icon").click()
        true_type_password = context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_password").\
            get_attribute("password")
        print("Password attribute is " + true_type_password)
        assert true_type_password == "true", "Password visualization is not correct: " + true_type_password

        print("Go back to avoid visible keyboard")
        context.driver.back()
    except Exception as e:
        assert False, e

    test_finished()


@when(u'Enter valid username and password')
def step_impl(context):
    email = "federico.capdeville@mirgor.com.ar"
    password = "12345a"

    print()

    try:
        # Timer to avoid the splash screen
        print('Avoid the splash screen timer')
        delay(5)

        print('Print email in application')
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_email").click()
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_email").send_keys(email)

        print('Print password in application')
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_password").click()
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_password").send_keys(password)

        print('Click to enable/disable the view of the password')
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/text_input_end_icon").click()
        false_type_password = context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_password"). \
            get_attribute("password")
        print("Password attribute is " + false_type_password)
        assert false_type_password == "false", "Password visualization is not correct: " + false_type_password

        context.driver.find_element_by_id("com.mirgor.aliciahome:id/text_input_end_icon").click()
        true_type_password = context.driver.find_element_by_id("com.mirgor.aliciahome:id/edit_login_password").\
            get_attribute("password")
        print("Password attribute is " + true_type_password)
        assert true_type_password == "true", "Password visualization is not correct: " + true_type_password

        print("Go back to avoid visible keyboard")
        context.driver.back()
    except Exception as e:
        assert False, e

    test_finished()


@then(u'Click continue with valid parameters')
def step_impl(context):
    print()

    try:
        print('Click to continue to the next screen')
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/button_login").click()
        delay(5)

        print("Check if it is the next screen")
        context.driver.find_element_by_id("com.mirgor.aliciahome:id/button_go_to_devices")
        delay(5)

    except Exception as e:
        assert False, e

    test_finished()
