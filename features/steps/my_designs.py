# This file is intended to describe functions, classes, methods and every element required for the entire automation
import time


def test_finished():
    print()
    print('Print of test_finished')
    pass


def delay(delay_time):
    time.sleep(delay_time)
    pass


def delay_until_appear(driver, element_id, delay_time=1, delay_timeout=20):
    delay_counter = 0
    while delay_counter < delay_timeout:
        time.sleep(delay_time)
        print('counter: ' + str(delay_counter))
        try:
            driver.find_element_by_id(element_id)
            return
        except Exception as error:
            delay_counter = delay_counter + 1
            print(error)

    raise Exception("Timeout generated. Could not wait until appear the element associated: " + element_id)
